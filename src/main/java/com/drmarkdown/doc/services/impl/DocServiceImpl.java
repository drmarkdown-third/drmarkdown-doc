package com.drmarkdown.doc.services.impl;

import com.drmarkdown.doc.daos.DocDAO;
import com.drmarkdown.doc.dtos.DocDTO;
import com.drmarkdown.doc.exceptions.DocDeleteException;
import com.drmarkdown.doc.exceptions.UserNotAllowedException;
import com.drmarkdown.doc.models.DocModel;
import com.drmarkdown.doc.services.DocService;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Objects.isNull;

@Service
@Slf4j
public class DocServiceImpl implements DocService {

    @Autowired
    DocDAO docDAO;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public void createDocument(DocDTO docDTO) {

        checkNotNull(docDTO.getContent());
        checkNotNull(docDTO.getTitle());
        checkNotNull(docDTO.getUserId());

        DocModel docModel = modelMapper.map(docDTO, DocModel.class);

        if(isNull(docModel.getAvailable())) {
            docModel.setAvailable(false);
        }

        docDAO.save(docModel);

        modelMapper.map(docModel, docDTO);
    }

    @Override
    public List<DocDTO> fetchDocsForUserId(String userID, String callerUserId) {

        final List<DocModel> allByUserId = docDAO.findAllByUserIdOrderByUpdatedAtDesc(userID);

        if (userID.equals(callerUserId)) {
            return allByUserId.stream()
                    .map(docModel -> modelMapper.map(docModel, DocDTO.class))
                    .collect(Collectors.toList());
        } else {
            return allByUserId.stream()
                    .filter(DocModel::getAvailable)
                    .map(docModel -> modelMapper.map(docModel, DocDTO.class))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public DocDTO fetchDoc(String docId, String userId) {

        final Optional<DocModel> optionalDocModel = docDAO.findById(docId);
        if (optionalDocModel.isPresent()) {

            if (optionalDocModel.get().getUserId().equals(userId)) {
                return modelMapper.map(optionalDocModel.get(), DocDTO.class);
            } else {
                if (optionalDocModel.get().getAvailable()) {
                    return modelMapper.map(optionalDocModel.get(), DocDTO.class);
                }
            }

            return null;
        }
        return null;
    }

    @Override
    public void deleteDocById(String docId) {

        Optional<DocModel> docModel = docDAO.findById(docId);

        if (docModel.isPresent()) {
            docDAO.deleteById(docId);
            log.info("Deleted doc with id `" + docId + "'");
        } else {
            throw new DocDeleteException("Theres no document with id '" + docId + "'");
        }
    }

    @Override
    public List<DocDTO> fetchTopRecentDocs() {

        final Page<DocModel> recentDocs = docDAO.findByAvailable(true, PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "updatedAt")));

        return recentDocs.stream()
                .map(docModel -> modelMapper.map(docModel, DocDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public void updateDoc(DocDTO docDTO, String userId) throws UserNotAllowedException {

        checkNotNull(docDTO.getContent());
        checkNotNull(docDTO.getTitle());
        checkNotNull(docDTO.getUserId());

        final Optional<DocModel> optionalDocModel = docDAO.findById(docDTO.getId());
        if (optionalDocModel.isPresent()) {

            final DocModel docModel = optionalDocModel.get();
            if (docModel.getUserId().equals(userId)) {

                modelMapper.map(docDTO, docModel);

                docDAO.save(docModel);

                modelMapper.map(docModel, docDTO);
                return;

            } else {
                throw new UserNotAllowedException("You're not allowed to modify this document");
            }
        }
        throw new NoSuchElementException("No such document with id " + docDTO.getId() + " was found");
    }
}
