package com.drmarkdown.doc.services;

import com.drmarkdown.doc.dtos.DocDTO;
import com.drmarkdown.doc.exceptions.UserNotAllowedException;

import java.util.List;

public interface DocService {
    void createDocument(DocDTO docDTO);

    List<DocDTO> fetchDocsForUserId(String userID, String callerUserId);

    DocDTO fetchDoc(String docId, String userId);

    void deleteDocById(String docId);

    List<DocDTO> fetchTopRecentDocs();

    void updateDoc(DocDTO docDTO, String userId) throws UserNotAllowedException;
}
